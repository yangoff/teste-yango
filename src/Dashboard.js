import React from 'react';
import { getUser, removeUserSession } from './Utils/Common';
import Chart from "react-google-charts";
import { Row,Col } from 'react-bootstrap';

function Dashboard(props) {
  const user = getUser();


  const handleLogout = () => {
    removeUserSession();
    props.history.push('/login');
  }

  return (
    <div>
      
      Welcome {user.name}!<br /><br />
      <input type="button" onClick={handleLogout} value="Logout" />
      
        <Row>
          <Col>
          <Chart
            width={'300px'}
            height={'300px'}
            chartType="AreaChart"
            loader={<div>Loading Chart</div>}
            data={[
              ['Year', 'Sales', 'Expenses'],
              ['2013', 1000, 400],
              ['2014', 1170, 460],
              ['2015', 660, 1120],
              ['2016', 1030, 540],
            ]}
            options={{
              title: 'Company Performance',
              hAxis: { title: 'Year', titleTextStyle: { color: '#333' } },
              vAxis: { minValue: 0 },
              // For the legend to fit, we make the chart area smaller
              chartArea: { width: '50%', height: '50%' },
              // lineWidth: 25
            }}
            // For tests
            rootProps={{ 'data-testid': '1' }}
          />
          </Col>
          <Col>
          <Chart
              width={'300px'}
              height={'300px'}
              chartType="BarChart"
              loader={<div>Loading Chart</div>}
              data={[
                ['City', '2010 Population', '2000 Population'],
                ['New York City, NY', 8175000, 8008000],
                ['Los Angeles, CA', 3792000, 3694000],
                ['Chicago, IL', 2695000, 2896000],
                ['Houston, TX', 2099000, 1953000],
                ['Philadelphia, PA', 1526000, 1517000],
              ]}
              options={{
                title: 'Population of Largest U.S. Cities',
                chartArea: { width: '50%' },
                hAxis: {
                  title: 'Total Population',
                  minValue: 0,
                },
                vAxis: {
                  title: 'City',
                },
              }}
              // For tests
              rootProps={{ 'data-testid': '1' }}
            />
          </Col>
          <Col>
            <Chart
              width={'300px'}
              height={'300px'}
              chartType="Bar"
              loader={<div>Loading Chart</div>}
              data={[
                ['Year', 'Sales', 'Expenses', 'Profit'],
                ['2014', 1000, 400, 200],
                ['2015', 1170, 460, 250],
                ['2016', 660, 1120, 300],
                ['2017', 1030, 540, 350],
              ]}
              options={{
                // Material design options
                chart: {
                  title: 'Company Performance',
                  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
              }}
              // For tests
              rootProps={{ 'data-testid': '2' }}
            />
          </Col>  
          <Col>
            <Chart
              width={'300px'}
              height={'300px'}
              chartType="BarChart"
              loader={<div>Loading Chart</div>}
              data={[
                ['City', '2010 Population', '2000 Population'],
                ['New York City, NY', 8175000, 8008000],
                ['Los Angeles, CA', 3792000, 3694000],
                ['Chicago, IL', 2695000, 2896000],
                ['Houston, TX', 2099000, 1953000],
                ['Philadelphia, PA', 1526000, 1517000],
              ]}
              options={{
                title: 'Population of Largest U.S. Cities',
                chartArea: { width: '50%' },
                isStacked: true,
                hAxis: {
                  title: 'Total Population',
                  minValue: 0,
                },
                vAxis: {
                  title: 'City',
                },
              }}
              // For tests
              rootProps={{ 'data-testid': '3' }}
            />
          </Col>
          <Col>
            <Chart
              width={'300px'}
              height={'300px'}
              chartType="CandlestickChart"
              loader={<div>Loading Chart</div>}
              data={[
                ['day', 'a', 'b', 'c', 'd'],
                ['Mon', 20, 28, 38, 45],
                ['Tue', 31, 38, 55, 66],
                ['Wed', 50, 55, 77, 80],
                ['Thu', 77, 77, 66, 50],
                ['Fri', 68, 66, 22, 15],
              ]}
              options={{
                legend: 'none',
              }}
              rootProps={{ 'data-testid': '1' }}
            /> 
          </Col>
          <Col>
          <Chart
              width={'300px'}
              height={'300px'}
              chartType="ComboChart"
              loader={<div>Loading Chart</div>}
              data={[
                [
                  'Month',
                  'Bolivia',
                  'Ecuador',
                  'Madagascar',
                  'Papua New Guinea',
                  'Rwanda',
                  'Average',
                ],
                ['2004/05', 165, 938, 522, 998, 450, 614.6],
                ['2005/06', 135, 1120, 599, 1268, 288, 682],
                ['2006/07', 157, 1167, 587, 807, 397, 623],
                ['2007/08', 139, 1110, 615, 968, 215, 609.4],
                ['2008/09', 136, 691, 629, 1026, 366, 569.6],
              ]}
              options={{
                title: 'Monthly Coffee Production by Country',
                vAxis: { title: 'Cups' },
                hAxis: { title: 'Month' },
                seriesType: 'bars',
                series: { 5: { type: 'line' } },
              }}
              rootProps={{ 'data-testid': '1' }}
            />
          </Col>
          <Col>
            <Chart
                width={'300px'}
                height={'300px'}
                chartType="Gantt"
                loader={<div>Loading Chart</div>}
                data={[
                  [
                    { type: 'string', label: 'Task ID' },
                    { type: 'string', label: 'Task Name' },
                    { type: 'date', label: 'Start Date' },
                    { type: 'date', label: 'End Date' },
                    { type: 'number', label: 'Duration' },
                    { type: 'number', label: 'Percent Complete' },
                    { type: 'string', label: 'Dependencies' },
                  ],
                  [
                    'Research',
                    'Find sources',
                    new Date(2015, 0, 1),
                    new Date(2015, 0, 5),
                    null,
                    100,
                    null,
                  ],
                  [
                    'Write',
                    'Write paper',
                    null,
                    new Date(2015, 0, 9),
                    3 * 24 * 60 * 60 * 1000,
                    25,
                    'Research,Outline',
                  ],
                  [
                    'Cite',
                    'Create bibliography',
                    null,
                    new Date(2015, 0, 7),
                    1 * 24 * 60 * 60 * 1000,
                    20,
                    'Research',
                  ],
                  [
                    'Complete',
                    'Hand in paper',
                    null,
                    new Date(2015, 0, 10),
                    1 * 24 * 60 * 60 * 1000,
                    0,
                    'Cite,Write',
                  ],
                  [
                    'Outline',
                    'Outline paper',
                    null,
                    new Date(2015, 0, 6),
                    1 * 24 * 60 * 60 * 1000,
                    100,
                    'Research',
                  ],
                ]}
                rootProps={{ 'data-testid': '1' }}
              />
          </Col>    
          <Col>
            <Chart
              width={'100%'}
              height={'400px'}
              chartType="Gantt"
              loader={<div>Loading Chart</div>}
              data={[
                [
                  { type: 'string', label: 'Task ID' },
                  { type: 'string', label: 'Task Name' },
                  { type: 'string', label: 'Resource' },
                  { type: 'date', label: 'Start Date' },
                  { type: 'date', label: 'End Date' },
                  { type: 'number', label: 'Duration' },
                  { type: 'number', label: 'Percent Complete' },
                  { type: 'string', label: 'Dependencies' },
                ],
                [
                  '2014Spring',
                  'Spring 2014',
                  'spring',
                  new Date(2014, 2, 22),
                  new Date(2014, 5, 20),
                  null,
                  100,
                  null,
                ],
                [
                  '2014Summer',
                  'Summer 2014',
                  'summer',
                  new Date(2014, 5, 21),
                  new Date(2014, 8, 20),
                  null,
                  100,
                  null,
                ],
                [
                  '2014Autumn',
                  'Autumn 2014',
                  'autumn',
                  new Date(2014, 8, 21),
                  new Date(2014, 11, 20),
                  null,
                  100,
                  null,
                ],
                [
                  '2014Winter',
                  'Winter 2014',
                  'winter',
                  new Date(2014, 11, 21),
                  new Date(2015, 2, 21),
                  null,
                  100,
                  null,
                ],
                [
                  '2015Spring',
                  'Spring 2015',
                  'spring',
                  new Date(2015, 2, 22),
                  new Date(2015, 5, 20),
                  null,
                  50,
                  null,
                ],
                [
                  '2015Summer',
                  'Summer 2015',
                  'summer',
                  new Date(2015, 5, 21),
                  new Date(2015, 8, 20),
                  null,
                  0,
                  null,
                ],
                [
                  '2015Autumn',
                  'Autumn 2015',
                  'autumn',
                  new Date(2015, 8, 21),
                  new Date(2015, 11, 20),
                  null,
                  0,
                  null,
                ],
                [
                  '2015Winter',
                  'Winter 2015',
                  'winter',
                  new Date(2015, 11, 21),
                  new Date(2016, 2, 21),
                  null,
                  0,
                  null,
                ],
                [
                  'Football',
                  'Football Season',
                  'sports',
                  new Date(2014, 8, 4),
                  new Date(2015, 1, 1),
                  null,
                  100,
                  null,
                ],
                [
                  'Baseball',
                  'Baseball Season',
                  'sports',
                  new Date(2015, 2, 31),
                  new Date(2015, 9, 20),
                  null,
                  14,
                  null,
                ],
                [
                  'Basketball',
                  'Basketball Season',
                  'sports',
                  new Date(2014, 9, 28),
                  new Date(2015, 5, 20),
                  null,
                  86,
                  null,
                ],
                [
                  'Hockey',
                  'Hockey Season',
                  'sports',
                  new Date(2014, 9, 8),
                  new Date(2015, 5, 21),
                  null,
                  89,
                  null,
                ],
              ]}
              options={{
                height: 400,
                gantt: {
                  trackHeight: 30,
                },
              }}
              rootProps={{ 'data-testid': '2' }}
            />
          </Col>
          <Col>
            <Chart
              width={'500px'}
              height={'300px'}
              chartType="GeoChart"
              data={[
                ['Country', 'Popularity'],
                ['Germany', 200],
                ['United States', 300],
                ['Brazil', 400],
                ['Canada', 500],
                ['France', 600],
                ['RU', 700],
              ]}
              // Note: you will need to get a mapsApiKey for your project.
              // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
              mapsApiKey="YOUR_KEY_HERE"
              rootProps={{ 'data-testid': '1' }}
            />
          </Col>
          <Col>
            <Chart
              width={'600px'}
              height={'400px'}
              chartType="LineChart"
              loader={<div>Loading Chart</div>}
              data={[
                ['x', 'dogs'],
                [0, 0],
                [1, 10],
                [2, 23],
                [3, 17],
                [4, 18],
                [5, 9],
                [6, 11],
                [7, 27],
                [8, 33],
                [9, 40],
                [10, 32],
                [11, 35],
              ]}
              options={{
                hAxis: {
                  title: 'Time',
                },
                vAxis: {
                  title: 'Popularity',
                },
              }}
              rootProps={{ 'data-testid': '1' }}
            />
          </Col>    
        
        </Row>

    </div>
    
  );
}

export default Dashboard;
